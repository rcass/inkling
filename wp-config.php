<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'inkling');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v>>aEVR>|78`Vv^X8e9I CW!`.GFH:zpwfx(WN5a8Azx?P)`)bigj>|CbwDrr B<');
define('SECURE_AUTH_KEY',  'bc&yG`#PP%43/R).}-J?uha6FDka7RW`Acm/%|!aCV(d(pm/{YRRE9n:wq1b53t|');
define('LOGGED_IN_KEY',    'BgYmFq5+yaX|`+g(jk_x<(D`w-.0xKA<+IU@?k2.e}:0FG9Q{lN<A-|ti|6Vb?tW');
define('NONCE_KEY',        'oq05Z|.:kJOKtH73FlIt!yOHzs3$yfq1a2Q~o?jFC6{RQH#|k!^6zZE+7|$n+@sM');
define('AUTH_SALT',        '5!^ir2D+:^32s!o$8zmT^O-dt&DtBW$7;*VW3o]TS^QRV)-W<-g.p7|T_m=}MqK1');
define('SECURE_AUTH_SALT', '+,;io4f|iWAE|V!<@}T]45F 7,H%ZI-+0##08([x#K>Opvq_D?|=vU9jTtrG-H!=');
define('LOGGED_IN_SALT',   'y2oXK}By3Q!|WOj1vIj>w @e.&f&]5Io<orR0}~`,b|+ms~(VTA,rQz?a|h|1TkW');
define('NONCE_SALT',       'kq]2iy-+9tw1Ss4cAPI yrhp)DlNat)#h,gv/O+M~O)7-JWBh)4Zc9lBP~h;RFm$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ink_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
