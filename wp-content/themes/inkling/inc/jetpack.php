<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package inkling
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function inkling_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'inkling_jetpack_setup' );
